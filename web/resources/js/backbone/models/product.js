
"use strict";
APP.ProductModel = Backbone.Model.extend({
    defaults: {
        name: "",
        cost: "0.00",
        duration: "0",
        amount: "0",
        timeOption: "minutes"
    },

    initialize: function(){
    },

    urlRoot : 'rest/products/',

    getIdFromUrl: function (response) {
        var productId = response._links.product.href.match(/.*\/products\/(.*)/)[1];
        return productId;
    },

    parse: function( response ) {
        var productId = this.getIdFromUrl(response);
        //console.log(productId);
        response.id = productId;
        return response;
    },

    validate: function (attrs) {
        var errors = {};
        if (!attrs.name) errors.name = "Hey! Give this product a title.";
        if (!attrs.cost) errors.cost = "You gotta write a cost, duh!";
        if (!attrs.duration) errors.duration = "Put duration";
        if (!_.isEmpty(errors)) return errors;
    }
});

APP.ProductCollection = Backbone.Collection.extend({
    model: APP.ProductModel,
    url : 'rest/products',

    parse: function ( response ) {
        return response._embedded.products;
    }
});
