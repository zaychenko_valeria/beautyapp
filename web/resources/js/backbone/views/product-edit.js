/**
 * Created by lera on 14.10.2016.
 */

"use strict";
APP.ProductEditView = Backbone.View.extend({
    // functions to fire on events
    events: {
        "click button.save": "save"
    },

    // the template
    template: _.template($('#formTemplate').html()),

    initialize: function (options) {
        this.model.bind('invalid', APP.helpers.showErrors, APP.helpers);
        this.model.bind('invalid', this.invalid, this);
    },

    invalid: function () {
        this.$el.find('a.cancel').hide();
    },

    save: function (event) {
        event.stopPropagation();
        event.preventDefault();

        // update our model with values from the form
        this.model.set({
            name: this.$el.find('input[name=name]').val(),
            cost: this.$el.find('input[name=cost]').val(),
            duration: this.$el.find('textarea[name=duration]').val(),
            amount:  this.$el.find('input[name=amount]').val(),
            timeOption:  this.$el.find('select[name=timeOption] option:selected').val()
        });

        if (this.model.isValid()) {
            this.model.save();
            // redirect back to the index
            Backbone.history.navigate('products/index', {trigger: true});
        }
    },

    // populate the html to the dom
    render: function () {
        this.$el.html(
            this.template(this.model.toJSON())
        );
        return this;
    }
});
