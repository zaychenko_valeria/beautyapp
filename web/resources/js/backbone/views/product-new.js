/**
 * Created by lera on 14.10.2016.
 */
"use strict";

APP.ProductNewView = Backbone.View.extend({
    // functions to fire on events
    // here we are blocking the submission of the form, and handling it ourself
    urlRoot : 'rest/products',

    events: {
        "click button.save": "save",
        "keyup input": "validate",
        "keyup textarea": "validate"
    },

    template: _.template($('#formTemplate').html()),

    initialize: function (options) {
        this.model.bind('invalid', APP.helpers.showErrors, APP.helpers);
    },

    save: function (event) {
        event.stopPropagation();
        event.preventDefault();

        // update our model with values from the form
        this.model.set({
            name: this.$el.find('input[name=name]').val(),
            cost: this.$el.find('input[name=cost]').val(),
            duration: this.$el.find('textarea[name=duration]').val(),
            amount:  this.$el.find('input[name=amount]').val(),
            timeOption:  this.$el.find('select[name=timeOption] option:selected').val()
        });

        if (this.model.isValid()) {
            // save it
            this.collection.add(this.model);
            this.model.save(//TODO
                {}, {success : function () {
                    Backbone.history.navigate("products/index", {trigger: true});
                }});
            //this.model.set(id, this.model.getIdFromUrl(response)
            // add it to the collection
            // redirect back to the index

        }
    },

    // populate the html to the dom
    render: function () {
        this.$el.html(
            this.template(this.model.toJSON())
        );
        return this;
    }
});
