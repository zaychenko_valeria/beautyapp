/**
 * Created by lera on 14.10.2016.
 */

"use strict";
APP.ProductIndexView = Backbone.View.extend({

    template: _.template($('#indexTemplate').html()),

    initialize: function () {
        this.listenTo( this.collection, 'update', this.render );
    },

    render: function () {
        this.$el.html(
            this.template({products: this.collection.toJSON()})
        );
        return this;
    }
});


