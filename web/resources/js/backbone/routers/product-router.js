/**
 * Created by lera on 14.10.2016.
 */
"use strict";

window.APP = window.APP || {};
APP.ProductRouter = Backbone.Router.extend({
    routes: {
        "product/new": "create",
        "products/index": "index",
        "products/:id/edit": "edit",
        "products/:id/delete": "delete"
    },

    $container: $('#primary-content'),

    initialize: function () {
        this.collection = new APP.ProductCollection();
        this.collection.fetch({ajaxSync: false});
        APP.helpers.debug(this.collection);
        this.index();
        // start backbone watching url changes
        Backbone.history.start();
    },

    create: function () {
        var view = new APP.ProductNewView({
            collection: this.collection,
            model: new APP.ProductModel()
        });
        this.$container.html(view.render().el);
    },

    delete: function (id) {
        var product = this.collection.get(id);
        product.destroy();
        Backbone.history.navigate("products/index", {trigger: true});
    },

    edit: function (id) {
        var view = new APP.ProductEditView({model: this.collection.get(id)});
        this.$container.html(view.render().el);
    },

    index: function () {
        var view = new APP.ProductIndexView({collection: this.collection});
        this.$container.html(view.render().el);
    }
});
