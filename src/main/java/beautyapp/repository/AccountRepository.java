package beautyapp.repository;

import beautyapp.domain.Account;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

/**
 * Created by lera on 10/1/2016.
 */
@RepositoryRestResource
public interface AccountRepository extends CrudRepository< Account, String > { }