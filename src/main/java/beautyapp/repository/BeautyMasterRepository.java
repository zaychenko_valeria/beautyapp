package beautyapp.repository;

import beautyapp.domain.BeautyMaster;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by lera on 10/3/2016.
 */
public interface BeautyMasterRepository extends CrudRepository< BeautyMaster, String > { }
