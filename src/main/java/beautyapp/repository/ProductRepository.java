package beautyapp.repository;


import beautyapp.domain.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * Created by lera on 10/4/2016.
 */
public interface ProductRepository extends CrudRepository< Product, String > { }
