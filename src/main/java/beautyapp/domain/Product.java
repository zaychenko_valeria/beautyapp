package beautyapp.domain;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by lera on 10/3/2016.
 */
@Entity(name = "product")
public class Product {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", unique = true)
    private String id;

    @Column(name = "productName")
    @NotNull(message = "{invalid.name}")
    @Size(min=1)
    private String name;

    @NotNull(message = "{invalid.cost}")
    private double cost;

    @NotNull(message = "{invalid.duration}")
    private double duration;//TODO add class Duration with int x & enum (hour, minute)

    @NotNull(message = "{invalid.duration}")
    @Embedded
    @JsonUnwrapped
    private Duration newDuration;

    //------------------------------------------------------------------------------------------------------------------
    public Product(){}

    public Product(String name, double cost, double duration, Duration newDuration){
        this.name = name;
        this.cost = cost;
        this.duration = duration;
        this.newDuration = newDuration;
    }

    //------------------------------------------------------------------------------------------------------------------
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getDuration() {
        return duration;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public Duration getNewDuration() {
        return newDuration;
    }

    public void setNewDuration(Duration newDuration) {
        this.newDuration = newDuration;
    }
}
