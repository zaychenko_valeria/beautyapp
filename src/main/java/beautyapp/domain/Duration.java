package beautyapp.domain;

import javax.persistence.Embeddable;

/**
 * Created by lera on 10/26/2016.
 */
@Embeddable
public class Duration {

    private int amount;
    private TimeOption timeOption;

    //------------------------------------------------------------------------------------------------------------------
    public Duration() {}

    public Duration(int amount, TimeOption timeOption) {
        this.amount = amount;
        this.timeOption = timeOption;
    }

    //------------------------------------------------------------------------------------------------------------------
    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public TimeOption getTimeOption() {
        return timeOption;
    }

    public void setTimeOption(TimeOption timeOption) {
        this.timeOption = timeOption;
    }
}
