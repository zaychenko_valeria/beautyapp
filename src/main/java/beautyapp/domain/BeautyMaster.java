package beautyapp.domain;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by lera on 10/3/2016.
 */
@Entity
public class BeautyMaster {

    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", unique = true)
    private String id;

    @OneToOne
    @NotNull
    @Valid
    private Account account;

    @NotNull(message = "{invalid.address}")
    @Size(min=4)
    private String address;

    @OneToMany
    @Valid
    private List<Product> products;

    //------------------------------------------------------------------------------------------------------------------
    public BeautyMaster(){}

    public BeautyMaster(Account account, String address){
        this.account = account;
        this.address = address;
        this.products = new ArrayList<Product>();
    }

    //------------------------------------------------------------------------------------------------------------------
    public String getId() {
        return id;
    }

    public String getAccountId() {
        return account.getId();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Iterator<Product> getProductsIterator() {
        return products.iterator();
    }

    public void addProduct(Product product) {
        this.products.add(product);
    }
}
