package beautyapp.domain;

/**
 * Created by lera on 10/26/2016.
 */
public enum TimeOption {

    MINUTES,
    HOURS
}
