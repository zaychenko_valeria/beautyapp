package beautyapp.config;

import beautyapp.config.custom_serialization.ProductDeserializer;
import beautyapp.config.custom_serialization.ProductSerializer;
import beautyapp.domain.Product;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleDeserializers;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.databind.module.SimpleSerializers;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@ComponentScan(
        basePackages = {"beautyapp.service.impl", "beautyapp.repository"},
        excludeFilters = @ComponentScan.Filter(Controller.class)
)
@EnableTransactionManagement(
        mode = AdviceMode.PROXY, proxyTargetClass = false,
        order = Ordered.LOWEST_PRECEDENCE)
public abstract class BaseRootContextConfiguration {

    @Bean
    public abstract LocalContainerEntityManagerFactoryBean entityManagerFactory();

    @Bean
    public PlatformTransactionManager transactionManager() {
        return new JpaTransactionManager(
                this.entityManagerFactory().getObject()
        );
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.configure(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE,
                false);
        return mapper;
    }

    /*@Override TODO check whether use annotations like @JsonSerialize
    protected void configureJacksonObjectMapper(ObjectMapper objectMapper) {
        objectMapper.registerModule(new SimpleModule("MyCustomModule") {
            @Override
            public void setupModule(SetupContext context) {
                SimpleSerializers serializers = new SimpleSerializers();
                SimpleDeserializers deserializers = new SimpleDeserializers();

                serializers.addSerializer(Product.class, new ProductSerializer());
                deserializers.addDeserializer(Product.class, new ProductDeserializer());

                context.addSerializers(serializers);
                context.addDeserializers(deserializers);
            }
        }
    }
    */
}
