package beautyapp.config.custom_serialization;

import beautyapp.domain.Duration;
import beautyapp.domain.Product;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;

/**
 * Created by lera on 10/26/2016.
 */
public class ProductSerializer extends StdSerializer<Product> {

    public ProductSerializer() {
        this(null);
    }

    public ProductSerializer(Class<Product> t) {
        super(t);
    }

    @Override
    public void serialize(Product value, JsonGenerator jgen, SerializerProvider provider)
            throws IOException, JsonProcessingException {
        jgen.writeStartObject();
        jgen.writeStringField("id", value.getId());
        jgen.writeStringField("name", value.getName());
        jgen.writeNumberField("cost", value.getCost());
        jgen.writeNumberField("duration", value.getDuration());
        jgen.writeNumberField("amount", value.getNewDuration().getAmount());
        jgen.writeStringField("timeOption", value.getNewDuration().getTimeOption().toString());
        jgen.writeEndObject();
    }
}
